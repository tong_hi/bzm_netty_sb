package com.fjb.tool.natives;

import java.lang.annotation.Annotation;
import java.lang.annotation.Native;

/**
 * @Description:Java语言无法访问操作系统底层信息（比如：底层硬件设备等），这时候就需要借助C语言来完成了。被native修饰的方法可以被C语言重写。
 * @author hemiao
 * @time:2020年4月18日 下午5:15:27
 */
public class NativeDemo {
	
	public static void main(String[] args) {
		
		// Object的底层用的是c语言写的
		Object object = new Object();
		
		Thread thread = new Thread();
		
	}
	
}
