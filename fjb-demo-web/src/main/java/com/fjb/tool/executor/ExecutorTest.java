package com.fjb.tool.executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * https://www.cnblogs.com/zsh-blogs/p/10963106.html
 * @Description:TODO
 * @author hemiao
 * @time:2020年4月13日 下午8:22:13
 */
public class ExecutorTest {
	
	// 执行标识
	private static boolean exeFlag = true;
	
	public static void main(String[] args) {
		
		// 创建ExecutorService 连接池创建固定的10个初始线程
		ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(10);
		
		AtomicInteger atomicInteger = new AtomicInteger();
		
		while (exeFlag){
		    if (atomicInteger.get() <= 100){
		    	newFixedThreadPool.execute(new Runnable() {
		            @Override
		            public void run() {
		                System.out.println("爬取了第"+atomicInteger.get()+"网页...");
		                atomicInteger.getAndIncrement();
		            }
		        });
		    }else {
		    	ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor)newFixedThreadPool;
		        if (threadPoolExecutor.getActiveCount() == 0){
		        	newFixedThreadPool.shutdown();
		            exeFlag=false;
		            System.out.println("爬虫任务已经完成");
		        }
		    }
		   try {	
			Thread.sleep((long) 0.1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		}
	}
	
}
