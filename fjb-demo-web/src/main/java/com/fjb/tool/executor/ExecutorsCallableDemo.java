package com.fjb.tool.executor;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @Description:Executor执行Callable任务
 * @author hemiao
 * @time:2020年4月13日 下午7:18:54
 */
public class ExecutorsCallableDemo {
	
	public static void main(String[] args) {
		
		ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
		
		ArrayList<Future<String>> futureList = new ArrayList<Future<String>>();
		
		//创建10个任务并执行 
		for (int i = 0; i < 10; i++) {
			// 使用ExecutorService执行Callable类型的任务，并将结果保存在future变量中  
			Future<String> future = cachedThreadPool.submit(new TaskWithResult(i));
			//将任务执行结果存储到List中   
			futureList.add(future);   
		}
		
		for (Future<String> future : futureList) {
			try {
				// Future返回如果没有完成，则一直循环等待，直到Future返回完成
				while (!future.isDone()) {
					// 打印各个线程（任务）执行的结果 
					System.out.println(future.get());
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			} finally {	
				//启动一次顺序关闭，执行以前提交的任务，但不接受新任务  
				cachedThreadPool.shutdown();   
			}   
		}
	}
	
}
