package com.fjb.tool.thread;

import java.util.concurrent.TimeUnit;

/**
 * @Description:线程例子
 * @author hemiao
 * @time:2020年4月7日 下午8:15:27
 */
public class ThreadDemo extends Thread{
	
	private static int i;

    public static void main(String[] args) throws InterruptedException {
    	Object obj = new Object();
    	obj.notify();
    	obj.notifyAll();
    	obj.wait();
    	obj.wait(2000);
    	obj.wait(2000, 3000);
    	
    	Thread t1 = new Thread();
    	t1.sleep(3000);
    	
    	
    	
    	new Thread(()->{
                i++;
            System.out.println("i:"+i);
        }).start();
  
    	Thread thread = new Thread();
    	thread.start();
    	thread.sleep(1000);
    	thread.join();
    	thread.notify();
    	thread.stop();
    	// 是否中断
    	boolean interrupted = thread.isInterrupted();
    	// 是否活跃
    	boolean alive = thread.isAlive();
    	// 是否守护进程
    	boolean daemon2 = thread.isDaemon();
    	
    	State state = thread.getState();
    	
    	
    	thread.interrupt();
    	
    	new Thread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
			}
		}).start();
    	
    }
}
